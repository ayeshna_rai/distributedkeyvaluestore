package com.keyvaluestoreservice.scheduler;

import com.keyvaluestoreservice.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HealthCheckScheduler {

    @Autowired
    @Qualifier("cacheService")
    private CacheService cacheService;

    // every 5 seconds
    @Async
    @Scheduled(initialDelay = 1000, fixedRate = 5000)
    public void runHealthCheck() {
        log.info("Health check started");
        cacheService.healthCheckForNodes();
        log.info("Health check completed");
    }

}
