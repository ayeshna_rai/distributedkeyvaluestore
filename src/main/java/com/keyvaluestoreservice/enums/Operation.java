package com.keyvaluestoreservice.enums;

public enum Operation {

    SET, GET, EXPIRE;

}
