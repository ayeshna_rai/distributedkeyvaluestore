package com.keyvaluestoreservice.enums;

public enum ErrorCodes {
    NOT_FOUND("NOT_FOUND"),
    KEY_NOT_FOUND("Key not found in the system"),
    NEW_CACHE_NOT_ADDED("New Cache Node could not be added to the Cache Service"),
    CACHE_NOT_DELETED("Cache Node could not be deleted from the Cache Service");

    private String message;

    ErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
