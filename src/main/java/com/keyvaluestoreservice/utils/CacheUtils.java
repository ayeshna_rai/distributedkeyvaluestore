package com.keyvaluestoreservice.utils;

import com.keyvaluestoreservice.cache.Cache;

import java.util.List;
import java.util.Random;
import java.util.Set;

public class CacheUtils {

    public static long hash(String word) {
        long hash = 7;
        for (int i = 0; i < word.length(); i++) {
            hash = hash * 31 + word.charAt(i);
        }
        return hash;
    }

    public static int index(long hash, Set<Integer> activeCache, List<Cache> caches) {
        int node = (int) (hash % caches.size());
        if (activeCache.contains(node))
            return node;
        return getAnyActiveCacheNode(activeCache);
    }

    private static int getAnyActiveCacheNode(Set<Integer> activeCache) {
        int i = new Random().nextInt(activeCache.size());
        while(i -- > 1)
            activeCache.iterator().next();
        return activeCache.iterator().next();
    }

}
