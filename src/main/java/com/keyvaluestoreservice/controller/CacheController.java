package com.keyvaluestoreservice.controller;

import com.keyvaluestoreservice.apimodel.KeyValueSetRequestModel;
import com.keyvaluestoreservice.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("")
public class CacheController {

    @Autowired
    @Qualifier("cacheService")
    private CacheService cacheService;

    @GetMapping("/get")
    public ResponseEntity<?> getValue(@RequestParam("key") String key) {
        return new ResponseEntity<>(cacheService.get(key), HttpStatus.OK);
    }

    @PostMapping("/expire")
    public ResponseEntity<?> expireKey(@RequestBody @Valid KeyValueSetRequestModel requestModel) {
        return new ResponseEntity<>(cacheService.expire(requestModel.getKey()), HttpStatus.OK);
    }

    @PostMapping("/set")
    public ResponseEntity<?> setKeyValue(@RequestBody @Valid KeyValueSetRequestModel requestModel) {
        return new ResponseEntity<>(cacheService.set(requestModel), HttpStatus.OK);
    }

}
