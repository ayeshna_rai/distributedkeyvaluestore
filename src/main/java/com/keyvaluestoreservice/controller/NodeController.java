package com.keyvaluestoreservice.controller;

import com.keyvaluestoreservice.service.CacheService;
import com.keyvaluestoreservice.service.NodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/nodes")
public class NodeController {

    @Autowired
    private NodeService nodeService;

    @Autowired
    @Qualifier("cacheService")
    private CacheService cacheService;

    @PostMapping("/add")
    public ResponseEntity<?> addNode() {
        return new ResponseEntity<>(nodeService.addNode(), HttpStatus.OK);
    }

    @PostMapping("/remove/{cacheId}")
    public ResponseEntity<?> removeNode(@PathVariable Integer cacheId) {
        return new ResponseEntity<>(nodeService.removeNode(cacheId), HttpStatus.OK);
    }

    @GetMapping("/get-all-nodes")
    public ResponseEntity<?> getAllNodes() {
        return new ResponseEntity<>(cacheService.getAllNodes(), HttpStatus.OK);
    }

}
