package com.keyvaluestoreservice.apimodel;

import com.google.gson.annotations.SerializedName;
import com.keyvaluestoreservice.enums.ErrorCodes;
import lombok.*;

import java.io.Serializable;

@Data
@ToString
@Builder
@NoArgsConstructor
public class ErrorResponse implements Serializable {

    @SerializedName("error_message")
    private String errorMessage;

    @SerializedName("error_code")
    private ErrorCodes errorCode;

    public ErrorResponse(String errorMessage, ErrorCodes errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }

    public ErrorResponse(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
