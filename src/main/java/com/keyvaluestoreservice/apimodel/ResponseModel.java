package com.keyvaluestoreservice.apimodel;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class ResponseModel implements Serializable {

    @SerializedName("error")
    private ErrorResponse errorResponse;

    @SerializedName("data")
    private Object data;

}
