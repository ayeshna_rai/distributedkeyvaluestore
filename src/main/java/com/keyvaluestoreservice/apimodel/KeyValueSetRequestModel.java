package com.keyvaluestoreservice.apimodel;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KeyValueSetRequestModel implements Serializable {

    @SerializedName("key")
    @NotNull
    private String key;

    @SerializedName("value")
    private Object value;

}
