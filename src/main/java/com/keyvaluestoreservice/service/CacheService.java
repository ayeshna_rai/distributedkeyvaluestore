package com.keyvaluestoreservice.service;

import com.keyvaluestoreservice.apimodel.ErrorResponse;
import com.keyvaluestoreservice.apimodel.KeyValueSetRequestModel;
import com.keyvaluestoreservice.apimodel.ResponseModel;
import com.keyvaluestoreservice.cache.Cache;
import com.keyvaluestoreservice.enums.ErrorCodes;
import com.keyvaluestoreservice.enums.Operation;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

import static com.keyvaluestoreservice.utils.CacheUtils.hash;
import static com.keyvaluestoreservice.utils.CacheUtils.index;

@Slf4j
public class CacheService {

    List<Cache> caches;
    Map<String, Integer> keyToCacheNodeMap;
    Set<Integer> activeCache;

    public CacheService(List<Cache> caches) {
        this.caches = caches;
        keyToCacheNodeMap = new HashMap<>();
        activeCache = new HashSet<>();
        for(int count = 0; count < caches.size(); count++)
            activeCache.add(count);
        log.info("Cache Service created");
    }

    public ResponseModel get(String key) {
        int cacheNode = getCacheNode(key, Operation.GET);
        if (cacheNode < 0) {
            ResponseModel responseModel = new ResponseModel();
            responseModel.setErrorResponse(new ErrorResponse(ErrorCodes.KEY_NOT_FOUND.getMessage(), ErrorCodes.NOT_FOUND));
            return responseModel;
        }
        return caches.get(cacheNode).get(key);
    }

    public ResponseModel expire(String key) {
        int cacheNode = getCacheNode(key, Operation.EXPIRE);
        if (cacheNode < 0) {
            ResponseModel responseModel = new ResponseModel();
            responseModel.setErrorResponse(new ErrorResponse(ErrorCodes.KEY_NOT_FOUND.getMessage(), ErrorCodes.NOT_FOUND));
            return responseModel;
        }
        return caches.get(cacheNode).expire(key);
    }

    public ResponseModel set(KeyValueSetRequestModel requestModel) {
        return caches.get(getCacheNode(requestModel.getKey(), Operation.SET)).set(requestModel);
    }

    public ResponseModel getAllNodes() {
        ResponseModel responseModel = new ResponseModel();
        List<String> data = new ArrayList<>();
        for (int activeCacheId : activeCache) {
            data.add(activeCacheId + ": " + caches.get(activeCacheId).toString());
        }
        responseModel.setData(data);
        return responseModel;
    }

    public boolean addCache() {
        caches.add(new Cache());
        return true;
    }

    public boolean removeCache(int cacheId) {
        caches.set(cacheId, null);
        return true;
    }

    private void removeFromActiveCache(int cacheId) {
        activeCache.remove(cacheId);
        log.info("Cache {} removed from active caches", cacheId);
    }

    public void healthCheckForNodes() {
        String key = Integer.MAX_VALUE + " " + Integer.MAX_VALUE;
        String value = Integer.MAX_VALUE + " " + Integer.MAX_VALUE;
        for(int count = 0; count < caches.size(); count++) {
            Cache cache = caches.get(count);
            if (cache == null) {
                if (activeCache.contains(count))
                    removeFromActiveCache(count);
                continue;
            }
            if ((cache.set(new KeyValueSetRequestModel(key, value)).getErrorResponse() != null)
                    || (cache.get(key).getErrorResponse() != null)
                    || (cache.expire(key).getErrorResponse() != null)) {
                removeFromActiveCache(count);
                continue;
            }
            if (!activeCache.contains(count)) {
                activeCache.add(count);
                log.info("Cache {} added to active caches", count);
            }
        }
    }

    private int getCacheNode(String key, Operation operation) {
        switch (operation) {
            case GET:
            case EXPIRE:
                if (keyToCacheNodeMap.containsKey(key) && activeCache.contains(keyToCacheNodeMap.get(key)))
                    return keyToCacheNodeMap.get(key);
            case SET:
                int index = index(hash(key), activeCache, caches);
                keyToCacheNodeMap.put(key, index);
                return index;
        }
        return -1;
    }

}
