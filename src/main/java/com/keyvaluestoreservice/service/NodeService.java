package com.keyvaluestoreservice.service;

import com.keyvaluestoreservice.apimodel.ErrorResponse;
import com.keyvaluestoreservice.apimodel.ResponseModel;
import com.keyvaluestoreservice.enums.ErrorCodes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import static com.keyvaluestoreservice.constants.ApplicationConstants.CACHE_NODE_DELETED_MSG;
import static com.keyvaluestoreservice.constants.ApplicationConstants.NEW_CACHE_NODE_ADDED_MSG;

@Slf4j
@Service
public class NodeService {

    @Autowired
    @Qualifier("cacheService")
    private CacheService cacheService;

    public ResponseModel addNode() {
        ResponseModel responseModel = new ResponseModel();
        if (cacheService.addCache())
            responseModel.setData(NEW_CACHE_NODE_ADDED_MSG);
        else
            responseModel.setErrorResponse(new ErrorResponse(ErrorCodes.NEW_CACHE_NOT_ADDED.getMessage()));
        return responseModel;
    }

    public ResponseModel removeNode(Integer cacheId) {
        ResponseModel responseModel = new ResponseModel();
        if (cacheService.removeCache(cacheId))
            responseModel.setData(CACHE_NODE_DELETED_MSG);
        else
            responseModel.setErrorResponse(new ErrorResponse(ErrorCodes.CACHE_NOT_DELETED.getMessage()));
        return responseModel;
    }

}
