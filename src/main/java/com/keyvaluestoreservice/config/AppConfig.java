package com.keyvaluestoreservice.config;

import com.keyvaluestoreservice.cache.Cache;
import com.keyvaluestoreservice.constants.ApplicationConstants;
import com.keyvaluestoreservice.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;

import java.util.ArrayList;
import java.util.List;

@Configuration
@Slf4j
public class AppConfig {

    @Bean("cacheService")
    @EventListener(ApplicationStartedEvent.class)
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public CacheService getCacheService() {
        int noOfNodes = ApplicationConstants.INITIAL_NUMBER_OF_NODES;
        List<Cache> caches = new ArrayList<>();
        while(noOfNodes --> 0) {
            caches.add(new Cache());
        }
        log.info(ApplicationConstants.INITIAL_NUMBER_OF_NODES + " cache nodes created");
        return new CacheService(caches);
    }

}
