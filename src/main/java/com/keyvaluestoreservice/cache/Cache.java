package com.keyvaluestoreservice.cache;

import com.keyvaluestoreservice.apimodel.ErrorResponse;
import com.keyvaluestoreservice.apimodel.KeyValueSetRequestModel;
import com.keyvaluestoreservice.apimodel.ResponseModel;
import com.keyvaluestoreservice.enums.ErrorCodes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Hashtable;
import java.util.Map;

@Component
@Scope("prototype")
@Slf4j
public class Cache {

    private Map<String, Object> map;

    public Cache() {
        map = new Hashtable<>();
        log.info("Cache started");
    }

    public ResponseModel get(String key) {
        ResponseModel responseModel = new ResponseModel();
        if (!map.containsKey(key))
            responseModel.setErrorResponse(new ErrorResponse(ErrorCodes.KEY_NOT_FOUND.getMessage(), ErrorCodes.NOT_FOUND));
        else
            responseModel.setData(map.get(key));
        return responseModel;
    }

    public ResponseModel expire(String key) {
        ResponseModel responseModel = new ResponseModel();
        if (!map.containsKey(key))
            responseModel.setErrorResponse(new ErrorResponse(ErrorCodes.KEY_NOT_FOUND.getMessage(), ErrorCodes.NOT_FOUND));
        else {
            Object value = map.remove(key);
            responseModel.setData(String.format("Key: %s, value: %s pair removed", key, value));
        }
        return responseModel;
    }

    public ResponseModel set(KeyValueSetRequestModel requestModel) {
        ResponseModel responseModel = new ResponseModel();
        String key = requestModel.getKey();
        Object value = requestModel.getValue();
        map.put(key, value);
        responseModel.setData(String.format("Key: %s, value: %s pair added", key, value));
        return responseModel;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String key : map.keySet()) {
            sb.append(key).append(": ").append(map.get(key)).append(", ");
        }
        return sb.toString();
    }

}
