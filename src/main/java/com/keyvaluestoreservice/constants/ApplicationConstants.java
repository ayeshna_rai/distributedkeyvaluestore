package com.keyvaluestoreservice.constants;

public class ApplicationConstants {

    public static final Integer INITIAL_NUMBER_OF_NODES = 5;

    public static final String NEW_CACHE_NODE_ADDED_MSG = "New Cache Node successfully added to the Cache Service";

    public static final String CACHE_NODE_DELETED_MSG = "Cache Node successfully deleted from the Cache Service";

}
