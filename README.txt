
DISTRIBUTED KEY VALUE STORE

Distributed Key Value Store is an in-memory distributed key-value data store.

INSTALLATION

Follow the steps below to run the code:
1. Install Java on your system
    MacOs: Refer to https://mkyong.com/java/how-to-install-java-on-mac-osx
    UbuntuOs: Refer to https://linuxize.com/post/install-java-on-ubuntu-18-04

2. Download the DistributedKeyValueStore.zip file and extract it

3. Go to the DistributedKeyValueStore folder's path

4. Go inside the DistributedKeyValueStore folder:
    cd DistributedKeyValueStore

5. Build the project using the following command:
    ./gradle build

6. Run the jar file using the following command:
    java -jar build/libs/DistributedKeyValueStore-1.0-SNAPSHOT.jar src/main/java/com/keyvaluestoreservice/KeyValueStoreServiceApplication


USAGE

Please use the following CURL commands on Postman to set, get and expire a key value pair

1. SET:

curl -X POST \
  http://localhost:8080/key-value-store/api/set \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 42a4d360-c264-4309-fa31-effe59481f7c' \
  -d '{
	"key": "firstkey",
	"value": "first value"
}'

2. GET:

curl -X GET \
  'http://localhost:8080/key-value-store/api/get?key=firstkey' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 6ba6dae4-532a-cd77-543b-016b332b1f2e'

3. EXPIRE:

curl -X POST \
  http://localhost:8080/key-value-store/api/expire \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 77ab71fd-c894-af16-a1b6-d450f837163a' \
  -d '{
	"key": "firstkey"
}'


TESTING FOR ADMINS

The following functions are provided to the admins to test the code:

1. Add a new node to the service

curl -X POST \
  http://localhost:8080/key-value-store/api/nodes/add \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 8aa4d540-7b8b-a7d4-2952-725c8493bd56'

2. Remove a node from the service (to test what happens if a node goes down)
Here, node id is given as a path variable in the url

curl -X POST \
  http://localhost:8080/key-value-store/api/nodes/remove/1 \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 3d3c9dfa-56b7-9af7-9827-bacf1eacc101'

3. API to see the status of all the active nodes at present and the data present in those nodes

curl -X GET \
  http://localhost:8080/key-value-store/api/nodes/get-all-nodes \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 797bcb33-63cb-3072-32da-f75bffbb5cf8'